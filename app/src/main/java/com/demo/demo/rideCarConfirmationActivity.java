package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.demo.demo.view.BaseActivity;

public class rideCarConfirmationActivity extends BaseActivity implements View.OnClickListener {
Button btnApprove;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_confirmation_activity);
        initiallization();
    }

    private void initiallization() {
        btnApprove=findViewById(R.id.btnApprove);
        btnApprove.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
   switch(view.getId()){
    case R.id.btnApprove:
        //Alert for Submit
        AlertLogoutPopUp();
        break;
      }
    }


    private void AlertLogoutPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.approval_submit, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        Button btnStartdriving=alertDialog.findViewById(R.id.btnStartdriving);
        btnStartdriving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(rideCarConfirmationActivity.this, rideLaterActivity.class));
            }
        });
       /* final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.approval_submit);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        Button btnStartdriving=dialog.findViewById(R.id.btnStartdriving);
        btnStartdriving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

        startActivity(new Intent(rideCarSelectionActivity.this, rideLaterActivity.class));
            }
        });*/
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}