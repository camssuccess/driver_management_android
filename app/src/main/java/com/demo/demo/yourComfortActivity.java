package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.demo.demo.view.BaseActivity;

public class yourComfortActivity extends BaseActivity implements View.OnClickListener {
    TextView btnText;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_comfort_activity);
        initillization();
    }

    private void initillization() {
        btnText=findViewById(R.id.btnText);
        SeekBar seekBar=findViewById(R.id.progressBar);
        //int seekBarValue= seekBar.getProgress();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(mActivity, "Seek bar progress is :" + progressChangedValue,
                        Toast.LENGTH_LONG).show();
            }
        });

        OnclickListener();
    }

    private void OnclickListener() {
        btnText.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnText:
                startActivity(new Intent(this, categoriesActivity.class));
                break;
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}
