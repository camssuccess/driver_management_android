package com.demo.demo;

import android.graphics.Camera;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.demo.view.BaseFragment;

public class cameraLeftFrgment extends BaseFragment implements View.OnClickListener {
    View rootView;
    ImageView  imageViewCamera;
    TextView tvText;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate( R.layout.camera_left_fragment, container, false );

        init();

        return rootView;
    }

    private void init() {
        imageViewCamera= rootView.findViewById(R.id.ivcamera);
        //SureFaceView

        tvText= rootView.findViewById(R.id.tvText);
        imageViewCamera.setOnClickListener(this);
        tvText.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.ivcamera:

                break;
            case R.id.tvText:
                break;



        }
    }

}
