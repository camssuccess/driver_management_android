package com.demo.demo;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.demo.view.BaseActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class rideLaterActivity extends BaseActivity implements View.OnClickListener {
    RecyclerView recyclerProfile,recyclerData;
    LinearLayoutManager llManagerProfile,llManagerData;
    ProfileAdapter profileAdapter;
    DataAdapter dataAdapter;
    LinearLayout llSpinner;
    DatePickerDialog picker;
TextView tvv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_later_activity);
        findViewById();
    }

    private void findViewById() {
        recyclerProfile=findViewById(R.id.recyclerProfile)  ;
        recyclerData=findViewById(R.id.recyclerData)  ;
        tvv=findViewById(R.id.tvv);
        llManagerProfile = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerProfile.setLayoutManager(llManagerProfile);

        llManagerData = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerData.setLayoutManager(llManagerData);

        dataAdapter = new DataAdapter();
        recyclerData.setAdapter(dataAdapter);
        recyclerData.setHasFixedSize(true);
        llSpinner = findViewById(R.id.llSpinner);
        llSpinner.setOnClickListener(this);
        profileAdapter = new ProfileAdapter();
        recyclerProfile.setAdapter(profileAdapter);
        recyclerProfile.setHasFixedSize(true);
    }

    @Override
    public void onClick(View view) {
      switch(view.getId()){
       case R.id.llSpinner:
        setNormalPicker();
        break;
      }
    }
    private void setNormalPicker() {
        //Simple Calender popup getdate,gatemonth,getYear in click of ok button
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        picker = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        tvv.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, year, month, day);
        picker.show();
    }
    public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

     /*   public DataAdapter(Activity activty) {
            //this.data = favlist;
            this.context=activty;
        }*/


        @Override
        public DataAdapter.DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.data_layout, parent, false);

            return new DataAdapter.DataHolder(view);
        }

        @Override
        public int getItemCount() {

            return 3;
        }

        @Override
        public void onBindViewHolder(final DataAdapter.DataHolder holder, final int position) {
        }


        public class DataHolder extends RecyclerView.ViewHolder {
            public DataHolder(View itemView) {
                super(itemView);



            }

        }
    }
    public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.profileHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

     /*   public DataAdapter(Activity activty) {
            //this.data = favlist;
            this.context=activty;
        }*/


        @Override
        public ProfileAdapter.profileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.layout_profile, parent, false);

            return new ProfileAdapter.profileHolder(view);
        }

        @Override
        public int getItemCount() {

            return 3;
        }

        @Override
        public void onBindViewHolder(final ProfileAdapter.profileHolder holder, final int position) {

            holder.llfirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(mActivity, rideLaterCarSelectionActivity.class));
                }
            });
        /*    holder.text_username.setText("Jessica Lim");

            holder.text_wallet.setText("RM 4");
            holder.txt_currency.setText("RM 8");
            holder.LinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(RideLaterActiity.this,Chat.class));
                }
            });*/
        }

        public class profileHolder extends RecyclerView.ViewHolder {

            LinearLayout llfirst;

            public profileHolder(View itemView) {
                super(itemView);

                //CustomImageview
                llfirst=itemView.findViewById(R.id.llfirst);

            }

        }
    }
}
