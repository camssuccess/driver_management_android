package com.demo.demo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.demo.view.BaseFragment;

public class cameraRightFragment extends BaseFragment implements View.OnClickListener {
    View rootView;
    ImageView imageViewCamera;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate( R.layout.camera_right_fragment, container, false );
        init();

        return rootView;
    }

    private void init() {
        imageViewCamera= rootView.findViewById(R.id.ivcamera);
        imageViewCamera.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.ivcamera:
            //camera With SurfaceView

                break;
        }
    }
}
