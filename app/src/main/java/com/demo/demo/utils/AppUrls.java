package com.demo.demo.utils;

public class AppUrls {
    public static final String baseUrl ="http://api.bazaarsestore.in/api/user/";
    public static final String userRegistration = baseUrl+"sign_up_step1";
    public static final String userAbout = baseUrl+"sign_up_step2";
    public static final String userOTPVarification = baseUrl+"otp_verification";
    public static final String userLogin = baseUrl+"login";
    public static final String resendotp = baseUrl+"resend_otp";
    public static final String forgetpassword = baseUrl+"forget_password";


}
