package com.demo.demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.demo.demo.utils.AppUrls;
import com.demo.demo.view.AppSettings;
import com.demo.demo.view.AppUtils;
import com.demo.demo.view.BaseActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;



public class loginActivity extends BaseActivity implements View.OnClickListener {
    Button btnLogin;
    TextView tvsignup;
    LoginButton loginButton;
    EditText etForgotEmail, etusername, etPassword;
    TextView tvForgotPassword;
    LinearLayout sign_in_google_ll, signup_fb_LL;
    RelativeLayout rlRegister;
    private CallbackManager callbackManager;
    private static final String TAG = "JustFriendz";
    private static final int RC_SIGN_IN = 1001;
    GoogleSignInClient googleSignInClient;
    private FirebaseAuth firebaseAuth;

    private static final String TWITTER_KEY = "YOUR TWITTER KEY";
    private static final String TWITTER_SECRET = "YOUR TWITTER SECRET";

    //Tags to send the username and image url to next activity using intent
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PROFILE_IMAGE_URL = "image_url";

    //Twitter Login Button
    TwitterLoginButton twitterLoginButton;
    String emaildId, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_preset);

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        configureGoogleClient();
        initiallization();
        onClickListener();


    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        loginActivity.super.onBackPressed();
                    }
                }).create().show();
    }

    private void initiallization() {
        //key from firebase
        /*https://fir-demo-project.firebaseapp.com/__/auth/handler*/
        etusername = findViewById(R.id.etusername);
        etPassword = findViewById(R.id.etpassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvsignup = findViewById(R.id.tvsignup);
        loginButton = findViewById(R.id.login_button);
        sign_in_google_ll = findViewById(R.id.sign_in_google_ll);
        signup_fb_LL = findViewById(R.id.signup_fb_LL);
        loginButton.setReadPermissions("email");
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        rlRegister = findViewById(R.id.rlRegister);
        twitterLoginButton = findViewById(R.id.twitterLogin);
      //  getText();
    }


    private void getText() {
        emaildId = etusername.getText().toString().trim();
        password = etPassword.getText().toString().trim();

    }


    private void onClickListener() {
        //OnclickListener
        btnLogin.setOnClickListener(this);
        tvsignup.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        sign_in_google_ll.setOnClickListener(this);
        rlRegister.setOnClickListener(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlRegister:
               moveToSignup();
                break;
            case R.id.tvForgotPassword:
                AlertLogoutPopUp();
                break;

            case R.id.sign_in_google_ll:
                googleSignIn();
                break;

            case R.id.signup_fb_LL:
                handleFBLogin();
                break;
            case R.id.btnLogin:
                  moveToLogin();
                break;
        }
    }

    private void moveToSignup() {
        startActivity(new Intent(mActivity,signActivity.class));
    }


    private void moveToLogin() {
        if(etusername.getText().toString().trim().isEmpty()){
            Toast.makeText(mActivity, "username can not be empty", Toast.LENGTH_SHORT).show();
        }else if(etPassword.getText().toString().trim().isEmpty()){
            Toast.makeText(mActivity, "password can not be empty", Toast.LENGTH_SHORT).show();
        }else {
            AppUtils.hideSoftKeyboard(mActivity);
            if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity))
            {
             AppUtils.showRequestDialog(mActivity);
          //    hitloginApi();
            }
            else
            {
                Toast.makeText(mActivity, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                //AppUtils.showErrorMessage(etUrName, getString(R.string.no_internet), this.mActivity);
            }
           /* if (etusername.getText().toString().trim().equals("user123") && etPassword.getText().toString().trim().equals("123456")) {
                AppSettings.putString(AppSettings.userName,"user123");
                AppSettings.putString(AppSettings.password,"123456");
                Log.v("loginuserName",AppSettings.getString(AppSettings.userName));
                startActivity(new Intent(mActivity, dashboardActivity.class));
            } else {
                Toast.makeText(mActivity, "please enter valid username and password", Toast.LENGTH_SHORT).show();
            }*/
        }
    }


    private void googleSignIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            // showToastMessage("Firebase Authentication failed:" + task.getException());
                        }
                    }
                });
    }


    private void configureGoogleClient() {
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                // for the requestIdToken, this is in the values.xml file that
                // is generated from your google-services.json
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        // Initialize Firebase Auth
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            Log.d(TAG, "Currently Signed in: " + currentUser.getEmail());
            // showToastMessage("Currently Logged in: " + currentUser.getEmail());
        }

    }


    private void handleFBLogin() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        if (isLoggedIn) {
            LoginManager.getInstance().logOut();
            return;
        }

        LoginManager.getInstance().logInWithReadPermissions(loginActivity.this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setFacebookData(loginResult);
                            }
                        });
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(loginActivity.this, "CANCELED", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(loginActivity.this, "ERROR" + exception.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void setFacebookData(final LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                // Application code
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                //  showToastMessage("Google Sign in Succeeded");
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.e(TAG, "Google sign in failed", e);
                // showToastMessage("Google Sign in Failed " + e);
            }
        }
    }

    private void AlertLogoutPopUp() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.popup_forgot, viewGroup, false);
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        ImageView ivCross = alertDialog.findViewById(R.id.ivCross);
        etForgotEmail = alertDialog.findViewById(R.id.etEmail);

        final TextView tvOkay = alertDialog.findViewById(R.id.tvOkay);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etForgotEmail.getText().toString().isEmpty()) {
                    Toast.makeText(mActivity, "Please enter your contact number", Toast.LENGTH_SHORT).show();
                } else if(etForgotEmail.getText().toString().trim().length()<10){
                    Toast.makeText(mActivity, "Please enter your contact number", Toast.LENGTH_SHORT).show();
                }else {
                     startActivity(new Intent(mActivity,forgetActivity.class));
                }
                onResume();
            }
        });





      /*  final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_forgot);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ImageView ivCross = dialog.findViewById(R.id.ivCross);
        etForgotEmail = dialog.findViewById(R.id.etEmail);

        final TextView tvOkay = dialog.findViewById(R.id.tvOkay);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvOkay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (etForgotEmail.getText().toString().isEmpty()) {
                    Toast.makeText(dialog.getContext(), "Please enter your email Id", Toast.LENGTH_SHORT).show();
                }*//* else if (!AppUtils.isValidEmail(etForgotEmail.getText().toString().isEmpty())) {
                    Toast.makeText(dialog.getContext(), "Please enter your valid email id", Toast.LENGTH_SHORT).show();
                }*//* else {
                    dialog.dismiss();

                }
                onResume();
            }
        });*/
    }


/*****************************Login Api***************************************/
    private void hitloginApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AboutApi", AppUrls.userLogin);
        AndroidNetworking.post(AppUrls.userLogin)
                .addBodyParameter("username",etusername.getText().toString().trim())
                .addBodyParameter("password",etPassword.getText().toString().trim())
                .addBodyParameter("device_token", AppUtils.getDeviceID(mActivity))
                .addBodyParameter("auth_key","camsapitaxi")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //parseLoginApi
                        parseLoginApi(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                        Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        // handle error
                        if (error.getErrorCode() != 0) {
                            Toast.makeText(mActivity, String.valueOf(error.getErrorCode()), Toast.LENGTH_SHORT).show();
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                             //AppUtils.showErrorMessage(etusername, String.valueOf(error.getErrorDetail()), mActivity);
                            Toast.makeText(mActivity, String.valueOf(error.getErrorDetail()), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void parseLoginApi(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            if(response.get("status").equals("success")){
                Toast.makeText(mActivity, ""+response.get("message"), Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(this,dashboardActivity.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



}