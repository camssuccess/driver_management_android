package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demo.demo.view.BaseActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

public class dashboardActivity extends BaseActivity implements View.OnClickListener {
    TextView tvid;
   RelativeLayout sumLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();

    }

    private void init() {

        sumLayout=findViewById(R.id.sumLayout);
        sumLayout.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sumLayout:
                startActivity(new Intent(this,rideActivity.class));
                break;
        }
    }

 /*   private void AlertCameraGallery() {
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dashboard_popup);
        dialog.setCancelable(true);
        dialog.show();
        TextView tvAppointments = dialog.findViewById(R.id.tvAppointments);


        tvAppointments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  dialog.dismiss();
                startActivity(new Intent(dashboardActivity.this, rideActivity.class));
            }
        });


    }*/
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}