package com.demo.demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.demo.demo.utils.AppUrls;
import com.demo.demo.view.AppSettings;
import com.demo.demo.view.AppUtils;
import com.demo.demo.view.BaseActivity;


import org.json.JSONException;
import org.json.JSONObject;

import static com.demo.demo.fragmentLeft.etUrName;

public class varificationActivity extends BaseActivity implements View.OnClickListener {
      Button btnText;
    TextView tvResendCode;
    CountDownTimer countDownTimer;
    EditText etOtp;
    String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varification);

        countDownTimer = new CountDownTimer(50000, 1000) {
            public void onTick(long millisUntilFinished) {
                tvResendCode.setText("seconds remaining: " + millisUntilFinished / 1000);
            }
            public void onFinish() {
                tvResendCode.setText("Resend Code");
            }
        }.start();
        initillization();
        init();
    }

    private void initillization() {
        btnText=findViewById(R.id.btnText);
        etOtp=findViewById(R.id.etOtp);
        btnText.setOnClickListener(this);
        //getText Via String
         otp=etOtp.getText().toString().trim();

    }

    private void init() {
        // btnVerify = rootView.findViewById(R.id.btnVerify);
        tvResendCode = findViewById(R.id.tvResendCode);
        etOtp = findViewById(R.id.etOtp);
        resendCode();
        onClickListener();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
    private void resendCode() {
        countDownTimer = new CountDownTimer(50000, 1000) {
            public void onTick(long millisUntilFinished) {
                tvResendCode.setText("seconds remaining: " + millisUntilFinished / 1000);
            }
            public void onFinish() {
                tvResendCode.setText("Resend Code");
            }
        }.start();
    }

    private void onClickListener() {
        tvResendCode.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnText:
                AppUtils.hideSoftKeyboard(mActivity);
                if(etOtp.getText().toString().isEmpty()) {
                    Toast.makeText(this, "please enter OTP", Toast.LENGTH_SHORT).show();
                }else {
                    AppUtils.hideSoftKeyboard(mActivity);
                    if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
                        AppUtils.showRequestDialog(mActivity);
                        hitVarificationApi();
                    } else {
                        AppUtils.showErrorMessage(etUrName, getString(R.string.no_internet), this.mActivity);
                    }
                }


                break;
            case R.id.tvResendCode:
              //  hitresendCodeApi();
                break;
        }
    }

    private void hitresendCodeApi() {
       AppUtils.showRequestDialog(mActivity);
       Log.v("AboutApi",AppUrls.resendotp);
       AndroidNetworking.post(AppUrls.resendotp)
               .addBodyParameter("mobile",AppSettings.getString(AppSettings.mobNo))
               .addBodyParameter("auth_key","camsapitaxi")
               .setPriority(Priority.HIGH)
               .build()
               .getAsJSONObject(new JSONObjectRequestListener() {
                   @Override
                   public void onResponse(JSONObject response) {
                       parseresendCodeApi(response);
                   }



                   @Override
                   public void onError(ANError anError) {

                   }
               });


    }
    private void parseresendCodeApi(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            if(response.get("status").equals("success")){
                Toast.makeText(mActivity, ""+response.get("message"), Toast.LENGTH_SHORT).show();
                AppUtils.showErrorMessage(etUrName, String.valueOf(response.get("code")), this.mActivity);
                AppUtils.showErrorMessage(etUrName, String.valueOf(response.get("message")), this.mActivity);
                startActivity(new Intent(this,varifyActivity.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }


    //===========================================================\\
    private void hitVarificationApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AboutApi", AppUrls.userOTPVarification);
        AndroidNetworking.post(AppUrls.userOTPVarification)
                .addBodyParameter("code",AppSettings.getString(AppSettings.code))
                .addBodyParameter("mobile",AppSettings.getString(AppSettings.mobNo))
                .addBodyParameter("user_id", AppSettings.getString(AppSettings.userId))
                .addBodyParameter("auth_key","camsapitaxi")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseVarificationApi(response);
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();

                        Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                        Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        // handle error
                        if (error.getErrorCode() != 0) {
                            //  AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            // AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }


    //===========================================================\\
    private void parseVarificationApi(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            if(response.get("status").equals("success")){
                Toast.makeText(mActivity, ""+response.get("message"), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this,varifyActivity.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}