package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.demo.demo.view.BaseActivity;

public class mainFragment extends BaseActivity implements View.OnClickListener {
    FragmentManager fragmentManager;
    private Fragment fragment;
    FrameLayout flDashboard;
    LinearLayout layoutDots;
    Button btnText;
    ImageView ivleft,ivRight;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);
        init();
        displayView(0);
    }

    private void init() {
        flDashboard = findViewById(R.id.frame);
        layoutDots=findViewById(R.id.layoutDots);
        ivleft=findViewById(R.id.ivleft);
        ivRight=findViewById(R.id.ivRight);
        btnText=findViewById(R.id.btnText);
        btnText.setText("SUBMIT");
        onClickListener();
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
    private void onClickListener() {
       layoutDots.setOnClickListener(this);
        ivleft.setOnClickListener(this);
       ivRight.setOnClickListener(this);
        btnText.setOnClickListener(this);

    }

    public void displayView(int position) {
        fragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
                fragment = new fragmentLeft();
                break;
            case 1:
                //fragment = new FeaturedFragment();
                fragment = new fragmentRight();
                break;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.frame, fragment)
                .addToBackStack("")
                .commit();
    }

    @Override
    public void onClick(View view) {
        Intent mIntent;
        switch (view.getId()) {
                case R.id.ivleft:
                btnText.setText("SUBMIT");
                displayView( 0);

                break;

                case R.id.ivRight:
                btnText.setText("VERIFY");
                displayView( 1 );
                break;

               case  R.id.btnText:
                   if(btnText.getText().toString().equals("SUBMIT")){
                       Toast.makeText(this, "submitted Successfully", Toast.LENGTH_SHORT).show();
                   }
                   else{
                       Toast.makeText(this, "varified successfully", Toast.LENGTH_SHORT).show();
                   }

                startActivity(new Intent(this, varifyActivity.class));
                break;

            default:
                break;
        }
    }



}
