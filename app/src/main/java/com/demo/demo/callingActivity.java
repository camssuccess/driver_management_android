package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.demo.demo.view.BaseActivity;

public class callingActivity extends BaseActivity implements View.OnClickListener {

    ImageView ivUserProfile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calling_activity);

        init();


    }

    private void init() {
        ivUserProfile = findViewById(R.id.ivUserProfile);
        ivUserProfile.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivUserProfile:
                //StartActivity
                startActivity(new Intent(callingActivity.this, rideCarConfirmationActivity.class));
                break;
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}
