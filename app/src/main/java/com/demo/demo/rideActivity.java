package com.demo.demo;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.demo.demo.view.AppUtils;
import com.demo.demo.view.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class rideActivity extends FragmentActivity implements View.OnClickListener, OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private LocationManager mLocationManager;
    //location
    public static Location mLocation;
    //Location permission
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;
    private static final int REQUEST_PERMISSIONS = 1;
    private static String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    TextView tvRideNow;
    String keyValue;
    LinearLayout lllinear;
    ImageView ivback;
    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    TextView tvdropLocation;
    TextView tvpickLocation,tvCancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride);
        init();
        if(getPermission()){

            setUpLocation();
        }

    }


    private void init() {
        tvRideNow=findViewById(R.id.tvRideNow);
        lllinear=findViewById(R.id.lllinear);
        ivback=findViewById(R.id.ivback);


        lllinear.setOnClickListener(this);
        ivback.setOnClickListener(this);
        tvRideNow.setOnClickListener(this);


     mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


     fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
       fetchLocation();
        open();
       // geocoder();
    }

    private void fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                 //   Toast.makeText(getApplicationContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    assert supportMapFragment != null;
                    supportMapFragment.getMapAsync(rideActivity.this);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.tvRideNow:
               startActivity(new Intent(this,bookingConfirmedActivity.class));
                break;
                case R.id.ivback:
                   onBackPressed();
                break;


        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    public void open(){

        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_resource);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ImageView ivPickup=dialog.findViewById(R.id.ivPickup);
        ImageView ivDropoff=dialog.findViewById(R.id.ivDropoff);
   /*     ivPickup.setImageResource(R.drawable.gradient);
        ivPickup.setImageResource(R.drawable.blackdrawable);*/
        tvdropLocation=dialog.findViewById(R.id.tvdropLocation);
        tvpickLocation=dialog.findViewById(R.id.tvPickLocation);

        RelativeLayout relativepickupLayout=dialog.findViewById(R.id.relativepickupLayout);
        RelativeLayout relativedropoffLayout=dialog.findViewById(R.id.relativedropoffLayout);
        RelativeLayout relativelocationpopupLayout=dialog.findViewById(R.id.locationpopup);
        relativelocationpopupLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           Intent i=new Intent(rideActivity.this,newLocateActivity.class);
           i.putExtra("data",keyValue);
           startActivity(i);
            }
        });
         relativedropoffLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(rideActivity.this,newLocateActivity.class));
            }
        });


         if(!(tvdropLocation.getText().toString().isEmpty() && tvpickLocation.getText().toString().isEmpty())){
             dialog();
         }
      }

    @Override
    public void onMapReady(GoogleMap googleMap) {
       LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
       // LatLng latLng=new LatLng(Double.parseDouble(String.valueOf(mLocation.getLatitude())),Double.parseDouble(String.valueOf(mLocation.getLongitude())));
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("I am here!");
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 1));
        googleMap.addMarker(markerOptions);
       googleMap.setMaxZoomPreference(10.0f);
       googleMap.setMinZoomPreference(10.0f);

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Circle circle = googleMap.addCircle(new CircleOptions()
                    .center(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                    .radius(10000)
                    .strokeColor(getColor(R.color.colorAccent))
                    .fillColor(getColor(R.color.lightGrey)));
        }*/
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                     fetchLocation();
                }
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            mLocation = location;
            Log.v("wdqdwdw1", String.valueOf(location));
            AppUtils.hideDialog();
            mLocationManager.removeUpdates(mLocationListener);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private boolean getPermission() {
        // BEGIN_INCLUDE(contacts_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);

        } else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);
            return true;

        }
        return false;
        // END_INCLUDE(contacts_permission_request)

    }


    private void setUpLocation() {

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        getCurrentLocation();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    rideActivity.this, 1000);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

    }
    private void getCurrentLocation() {
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Location location = null;
        if (!(isGPSEnabled || isNetworkEnabled)) {
            Toast.makeText(rideActivity.this, "Please Open GPS", Toast.LENGTH_SHORT).show();
        } else {
            if (isNetworkEnabled) {
                if (ActivityCompat.checkSelfPermission(rideActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(rideActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE,
                        mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (isGPSEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            }
        }
     /*   if (location != null) {
            location.getLongitude();
            Toast.makeText(getLocation, String.valueOf(location.getLatitude()), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(mActivity, MapsActivity.class);
            intent.putExtra("lat", String.valueOf(location.getLatitude()));
            intent.putExtra("long", String.valueOf(location.getLongitude()));
            startActivity(intent);
            hitSetCurentLocationApi();
            Log.v("wdqdwdw", String.valueOf(location));
            Log.v("latLongdata",String.valueOf(location.getLatitude()));
        }*/
    }

    private android.location.LocationListener mLocationListener = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {

                mLocation = location;
                Log.v("wdqdwdw1", String.valueOf(location));
                getAddress();
              //  hitSetCurentLocationApi();

                AppUtils.hideDialog();

                mLocationManager.removeUpdates(mLocationListener);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }
        @Override
        public void onProviderDisabled(String provider) {
        }
    };
    public void getAddress() {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            fetchLocation();
            addresses = geocoder.getFromLocation(Double.parseDouble(String.valueOf(mLocation.getLatitude())),Double.parseDouble(String.valueOf(mLocation.getLongitude())), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //  Toast.makeText(mActivity, address, Toast.LENGTH_SHORT).show();
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
         //  preferences.set(Constants.userCurrentLocation, address);
            //preferences.commit();


            tvdropLocation.setText("No Location Selected");
            tvpickLocation.setText(address);
         //   Toast.makeText(this, ""+address, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   private void dialog(){
      Dialog d = new BottomSheetDialog(this);
                d.setContentView(R.layout.dailog);
                d.setCancelable(true);
                d.show();
          Button button=d.findViewById(R.id.btnBottomSheetDialog);
                button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    });
}
}