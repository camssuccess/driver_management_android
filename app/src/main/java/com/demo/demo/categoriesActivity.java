package com.demo.demo;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.demo.demo.view.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class categoriesActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView rcvBrands;
    LinearLayoutManager llManager;
    brandsAdapter brandsAdapter;
    Button btnDone;
    ImageView ivback;

    private ArrayList<HashMap<String, String>> categoryList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        initiallization();

    }

    private void initiallization() {
        rcvBrands = findViewById(R.id.rcvBrands);
        btnDone = findViewById(R.id.btnDone);
        ivback = findViewById(R.id.ivback);
        llManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvBrands.setLayoutManager(llManager);

        HashMap<String, String> hashlist = new HashMap();
        hashlist.put("BMW", "BMW");
        hashlist.put("SWIFT", "swift");
        hashlist.put("HAYABUSA", "HAYABUSA");
        hashlist.put("AVENGER", "AVENGER");
        hashlist.put("MARUTI", "MARUTI");


        //Hash List.put for adding data in recyclerView
        categoryList.add(hashlist);
        brandsAdapter = new brandsAdapter(this, categoryList);
        rcvBrands.setAdapter(brandsAdapter);
        rcvBrands.setHasFixedSize(true);
        OnclickListener();
    }

    private void OnclickListener() {
        btnDone.setOnClickListener(this);
        ivback.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDone:
                Intent i = new Intent(categoriesActivity.this, dashboardActivity.class);
                startActivity(i);
                break;
            case R.id.ivback:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
    // super.onBackPressed();
    }

    public class brandsAdapter extends RecyclerView.Adapter<brandsAdapter.brandsHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public brandsAdapter(Activity activty, ArrayList<HashMap<String, String>> favlist) {
            this.data = favlist;
        }


        @Override
        public brandsAdapter.brandsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.brands_list, parent, false);

            return new brandsAdapter.brandsHolder(view);

        }

        @Override
        public int getItemCount() {

            return data.size();
        }

        @Override
        public void onBindViewHolder(@NonNull final brandsAdapter.brandsHolder holder, final int position) {
            holder.txtName.setText(data.get(position).get("BMW"));
            holder.textView.setText(data.get(position).get("SWIFT"));

            //Holder data actual data
            holder.textView.setText(data.get(position).get("SWIFT"));

            holder.checked.setVisibility(View.GONE);
            holder.Unchecked.setVisibility(View.VISIBLE);
            holder.ivChecked2.setVisibility(View.GONE);
            holder.ivUnChecked2.setVisibility(View.VISIBLE);

            /*holder*/
            holder.llVarify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.checked.setVisibility(View.VISIBLE);
                    holder.Unchecked.setVisibility(View.GONE);
                }
            });

            holder.llnewVarify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.ivUnChecked2.setVisibility(View.GONE);
                    holder.ivChecked2.setVisibility(View.VISIBLE);
                }
            });


        }

        public class brandsHolder extends RecyclerView.ViewHolder {

            private TextView txtName;
            private TextView textView;
            private ImageView checked, Unchecked, ivChecked2,
                    ivUnChecked2;
            LinearLayout llnewVarify, llVarify;


            public brandsHolder(View itemView) {
                super(itemView);
                txtName = itemView.findViewById(R.id.brand1);
                textView = itemView.findViewById(R.id.brand2);
                checked = itemView.findViewById(R.id.ivChecked);
                Unchecked = itemView.findViewById(R.id.ivUnChecked);
                ivChecked2 = itemView.findViewById(R.id.ivChecked2);
                ivUnChecked2 = itemView.findViewById(R.id.ivUnChecked2);
                llnewVarify = itemView.findViewById(R.id.llnewVarify);
                llVarify = itemView.findViewById(R.id.llVarify);
            }

        }
    }

}