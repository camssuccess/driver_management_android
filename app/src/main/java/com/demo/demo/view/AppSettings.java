package com.demo.demo.view;

import android.app.Activity;

public final class AppSettings extends OSettings {
    public static final String PREFS_MAIN_FILE  = "PREFS_KNOSTICS_FILE";
    public static final String loginCheck = "loginCheck";
    public static final String loginId = "loginId";


    public static final String name = "name";
    public static final String profile_image = "profile_image";
    public static final String language = "language";
    public static final String language_id = "language_id";
    public static final String image_path = "image_path";
    public static final String fcmId = "fcmId";

    // rideId will use when driver start a ride
    public static final String rideId = "rideId";

    public static final String type = "type";

    public static final String Count="0";

    public static final String startRideTimestamp="startRideTimestamp";
    public static final String startRideReading="startRideReading";

//LoginActivity

    public static final String userName = "userName";
    public static final String password = "password";
    public static final String userId = "userId";
    public static final String mobNo = "mobNo";
    public static final String code = "code";






    public AppSettings(Activity mActivity) {
        super(mActivity);
    }
}
