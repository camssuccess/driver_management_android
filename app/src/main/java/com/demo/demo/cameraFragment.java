package com.demo.demo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.demo.demo.view.BaseActivity;

public class cameraFragment extends BaseActivity implements View.OnClickListener {
    FragmentManager fragmentManager;
    private Fragment fragment;
    FrameLayout flDashboard;
    Button btnText;
    LinearLayout layoutDots;
    ImageView ivleft,ivRight;
    String   ivStrleft, ivStrRight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_fragment);
        init();
        displayView(0);

    }

    private void init() {
        flDashboard = findViewById(R.id.frame);
        layoutDots=findViewById(R.id.layoutDots);
        ivleft=findViewById(R.id.ivleft);
        ivRight=findViewById(R.id.ivRight);
        btnText=findViewById(R.id.btnText);
        //dynamic
        btnText.setText(getString(R.string.submit));
        onClickListener();
    }

    private void onClickListener() {
        //OnclickListener
        layoutDots.setOnClickListener(this);
        ivleft.setOnClickListener(this);
        ivRight.setOnClickListener(this);
        btnText.setOnClickListener(this);

    }


    public void displayView(int position) {
        fragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
                fragment = new cameraLeftFrgment();
                break;
            case 1:
                //fragment = new FeaturedFragment();
                fragment = new cameraRightFragment();
                break;
            case 2:
                fragment = new cameraNextFrgment();
                break;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.frame, fragment)
                .addToBackStack("")
                .commit();
    }
    @Override
    public void onClick(View view) {

            switch (view.getId()) {
                case R.id.ivleft:
                    btnText.setText(getString(R.string.strVarify));
                    displayView( 0);
                    break;

                case R.id.ivRight:
                    btnText.setText("VERIFY");
                    displayView( 2 );
                    break;

                case  R.id.btnText:
                    if(btnText.getText().toString().equals("VERIFY")){
                        Toast.makeText(this, getString(R.string.varifymessage), Toast.LENGTH_SHORT).show();
                    }
                    else if(btnText.getText().toString().equals("VERIFY")) {
                        Toast.makeText(this, getString(R.string.varified), Toast.LENGTH_SHORT).show();
                    }
                    startActivity(new Intent(this,driverComfortActivity.class));
                    break;
                default:
                    break;
            }
        }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}