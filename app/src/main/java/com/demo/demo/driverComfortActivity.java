package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.demo.demo.view.BaseActivity;

public class driverComfortActivity extends BaseActivity implements View.OnClickListener {
Button buttonSelect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_comfort);

        init();
    }

    private void init() {
        buttonSelect=findViewById(R.id.buttonSelect);
        onClicklIstner();
    }

    private void onClicklIstner() {
        buttonSelect.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.buttonSelect:
                startActivity(new Intent(this, yourComfortActivity.class));
                break;
        }
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}