package com.demo.demo;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.demo.demo.view.BaseActivity;

public class varifyActivity extends BaseActivity implements View.OnClickListener {
    TextView buttonVarify;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.varify_activity);

        init();

    }

    private void init() {
        buttonVarify = findViewById(R.id.buttonVarify);
        onClickListener();
    }
    private void onClickListener() {
        buttonVarify.setOnClickListener(this);
    }

    @Override
       public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonVarify:
                AlertLogoutPopUp();
        }
    }

    private void AlertLogoutPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_activity, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        TextView textView = alertDialog.findViewById(R.id.tvnote3);
        TextView tvnote2 = alertDialog.findViewById(R.id.tvnote2);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(varifyActivity.this, cameraFragment.class));

            }
        });
        tvnote2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(varifyActivity.this, cameraFragment.class));
            }
        });
       /* final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_activity);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView textView = dialog.findViewById(R.id.tvnote3);
        TextView tvnote2 = dialog.findViewById(R.id.tvnote2);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             startActivity(new Intent(varifyActivity.this, cameraFragment.class));
               *//* startActivity(new Intent(Varify.this, SurFaceActivity.class));*//*
            }
        });
        tvnote2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              *//*  startActivity(new Intent(Varify.this, SurFaceActivity.class));*//*
                startActivity(new Intent(varifyActivity.this, cameraFragment.class));
            }
        });*/
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }


}
