package com.demo.demo;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.demo.view.BaseFragment;


public class fragmentRight extends BaseFragment implements View.OnClickListener {
    View rootView;
    TextView tvResendCode;
    CountDownTimer countDownTimer;
    EditText etOtp;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_right, container, false);

        init();
        countDownTimer = new CountDownTimer(50000, 1000) {
            public void onTick(long millisUntilFinished) {
                tvResendCode.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tvResendCode.setText("Resend Code");
            }
        }.start();
        return rootView;/**/

    }

    private void init() {
        // btnVerify = rootView.findViewById(R.id.btnVerify);
        tvResendCode = rootView.findViewById(R.id.tvResendCode);
        etOtp = rootView.findViewById(R.id.etOtp);
        resendCode();
        onClickListener();
    }

    private void resendCode() {
        countDownTimer = new CountDownTimer(50000, 1000) {
            public void onTick(long millisUntilFinished) {
                tvResendCode.setText("seconds remaining: " + millisUntilFinished / 1000);
            }
            public void onFinish() {
                tvResendCode.setText("Resend Code");
            }
        }.start();
    }

    private void onClickListener() {
        tvResendCode.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvResendCode:
                //resend Code
                resendCode();
                break;
            /*case R.id.btnVerify:
                if (etOtp.getText().toString().isEmpty()) {
                    Toast.makeText(mActivity, "Please enter otp ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mActivity, "OTP Verified.", Toast.LENGTH_SHORT).show();

                    Intent i=new Intent(getActivity(), varifyActivity.class);
                    startActivity(i);
                }*/
            // break;
        }
    }
}
