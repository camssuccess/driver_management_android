package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.demo.demo.view.BaseActivity;

public class rideLaterCarSelectionActivity extends BaseActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    ImageView ivLeft,ivRight;

    //All SeekBar
    private static final int TOTAL_AMOUNT = 100;


    private int[] mAllProgress = { 0, 0, 0, 0, 0,0,0 };

    int progressChangedValue = 0;
    SeekBar seekBar, seekBar1, seekBar2, seekBar4, seekBar8, seekBar41,seekBar81;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_class);
        //setContentView(R.layout.ride_later_car_selection);
        Initiallization();

    }

    private void Initiallization() {
        ivLeft =findViewById(R.id.ivLeft);
        ivRight=findViewById(R.id.ivRight);

         //OnClickListener
          ivLeft.setOnClickListener(this);
          ivRight.setOnClickListener(this);
         seekBar    =findViewById(R.id.progressBar);
         seekBar1   =findViewById(R.id.progressBar1);
         seekBar2   =findViewById(R.id.progressBar2);
         seekBar4   =findViewById(R.id.progressBar4);
         seekBar8   =findViewById(R.id.progressBar8);
         seekBar41  =findViewById(R.id.progressBar41);
         seekBar81  =findViewById(R.id.progressBar81);

/*manually storing progress*/
         seekBar  .setOnSeekBarChangeListener(this);
         seekBar1 .setOnSeekBarChangeListener(this);
         seekBar2 .setOnSeekBarChangeListener(this);
         seekBar41 .setOnSeekBarChangeListener(this);
         seekBar81 .setOnSeekBarChangeListener(this);

    /*    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(mActivity, "Seek bar progress is :" + progressChangedValue,
                        Toast.LENGTH_LONG).show();
            }
        });

        seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(mActivity, "Seek bar progress is :" + progressChangedValue,
                        Toast.LENGTH_LONG).show();
            }
        });*/

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
             case R.id.ivLeft:
                onBackPressed();
                break;
             case R.id.ivRight:
                startActivity(new Intent(this, rideLaterConfirmationActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        // find out which SeekBar triggered the event so we can retrieve its saved current

        progressChangedValue = progress;

        int which = whichIsIt(seekBar.getId());
        // the stored progress for this SeekBar
        int storedProgress = mAllProgress[which];

        // we basically have two cases, the user either goes to the left or to
        // the right with the thumb. If he goes to the right we must check to
        // see how much he's allowed to go in that direction(based on the other
        // SeekBar values) and stop him if he the available progress was used. If
        // he goes to the left use that progress as going back
        // and freeing the track isn't a problem.
        if (progress > storedProgress) {
            progressChangedValue=remaining();
            // how much is currently available based on all SeekBar progress
            int remaining = remaining();
            // if there's no progress remaining then simply set the progress at
            // the stored progress(so the user can't move the thumb further)
            if (remaining == 0) {
                seekBar.setProgress(storedProgress);
                return;
            } else {
                // we still have some progress available so check that available
                // progress and let the user move the thumb as long as the
                // progress is at most as the sum between the stored progress
                // and the maximum still available progress
                if (storedProgress + remaining >= progress) {
                    mAllProgress[which] = progress;
                } else {
                    // the current progress is bigger then the available
                    // progress so restrict the value
                    mAllProgress[which] = storedProgress + remaining;
                }
            }
        } else if (progress <= storedProgress) {
            // the user goes left so simply save the new progress(space will be
            // available to other SeekBars)
            mAllProgress[which] = progress;
        }
    }

    /**
     * Returns the still available progress after the difference between the
     * maximum value(TOTAL_AMOUNT = 100) and the sum of the store progresses of
     * all SeekBars.
     *
     * @return the available progress.
     */


    private int whichIsIt(int id) {
        switch (id) {
            case R.id.progressBar:

                Toast.makeText(mActivity, "seekBar remainig value"+remaining(), Toast.LENGTH_SHORT).show();
               /* Toast.makeText(mActivity, "50%", Toast.LENGTH_SHORT).show();*/
                return 0; // first position in mAllProgress
            case R.id.progressBar1:
                Toast.makeText(mActivity, "seekBar remainig value"+remaining(), Toast.LENGTH_SHORT).show();

                return 1;
            case R.id.progressBar2:
                Toast.makeText(mActivity, "seekBar remainig value"+remaining(), Toast.LENGTH_SHORT).show();

                return 2;
            case R.id.progressBar4:
                Toast.makeText(mActivity, "seekBar remainig value"+remaining(), Toast.LENGTH_SHORT).show();

                return 3;
            case R.id.progressBar8:
                Toast.makeText(mActivity, "seekBar remainig value"+remaining(), Toast.LENGTH_SHORT).show();

                return 4;
            case R.id.progressBar41:
                Toast.makeText(mActivity, "seekBar remainig value"+remaining(), Toast.LENGTH_SHORT).show();

                return 5;
            case R.id.progressBar81:
                Toast.makeText(mActivity, "seekBar remainig value"+remaining(), Toast.LENGTH_SHORT).show();

                return 6;
            default:
                throw new IllegalStateException("There should be a Seekbar with this id(" + id + ")!");
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
     /*   Toast.makeText(mActivity, "Seek bar progress is :" + progressChangedValue,
                Toast.LENGTH_SHORT).show();*/
    }

    private final int remaining() {

        int remaining = TOTAL_AMOUNT;
        for (int i = 0; i < 7; i++) {
            remaining -= mAllProgress[i];
        }
        if (remaining >= 100) {
            remaining = 100;
        } else if (remaining <= 0) {
            remaining = 0;
        }
        return remaining;
    }
}
