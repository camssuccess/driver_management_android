package com.demo.demo;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.demo.demo.view.BaseActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

public class bookingConfirmedActivity extends BaseActivity implements View.OnClickListener {

    Button btnGoBackToLocation;
    double latitude = 20.3440000, longitude = 34.3400000;
    double destinationLatitude = 20.5666, destinationLongitude = 45.345;
    LayoutInflater v;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);
        initillization();
    }

    private void initillization() {
        btnGoBackToLocation = findViewById(R.id.btnGoBackToLocation);
        btnGoBackToLocation.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGoBackToLocation:
                AlertLogoutPopUp();
                break;
        }
    }

    private void AlertLogoutPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.booking_confirmation_popup, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
        ImageView ivProfile=alertDialog.findViewById(R.id.ivProfile);
        SeekBar seekBar=alertDialog.findViewById(R.id.progressBar);
        SeekBar seekBar2=alertDialog.findViewById(R.id.progressBar2);
        SeekBar seekBar3=alertDialog.findViewById(R.id.progressBar3);
        //int seekBarValue= seekBar.getProgress();

        //seekbar OnCheckChangeListener
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(mActivity, "Seek bar progress is :" + progressChangedValue,
                        Toast.LENGTH_LONG).show();
            }
        });

        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue2 = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue2 = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(mActivity, "Seek bar progress is :" + progressChangedValue2,
                        Toast.LENGTH_LONG).show();
            }
        });

        seekBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue3 = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue3 = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(mActivity, "Seek bar progress is :" + progressChangedValue3,
                        Toast.LENGTH_LONG).show();
            }
        });


        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(bookingConfirmedActivity.this, passangerActivity.class));

            }
        });

    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
}
