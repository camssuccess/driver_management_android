package com.demo.demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.demo.demo.utils.AppUrls;
import com.demo.demo.view.AppSettings;
import com.demo.demo.view.AppUtils;
import com.demo.demo.view.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import static com.demo.demo.fragmentLeft.etUrName;


public class forgetActivity extends BaseActivity implements View.OnClickListener {
    EditText etForgetConfirmOTP,etForgetCurrentOTP,etOTP;
    Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_activity);

        findViewById();
    }

    private void findViewById() {
        etForgetConfirmOTP=findViewById(R.id.etForgetConfirmOTP);
        etForgetCurrentOTP=findViewById(R.id.etForgetCurrentOTP);
        etOTP=findViewById(R.id.etForgetOTP);
        btnSubmit=findViewById(R.id.SUBMIT);

        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.SUBMIT:
                if(etOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, "OTP can not be empty", Toast.LENGTH_SHORT).show();
                }else if(etForgetCurrentOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, "current otp can not be empty", Toast.LENGTH_SHORT).show();
                }else if(etForgetConfirmOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, "confirm otp can not be empty", Toast.LENGTH_SHORT).show();
                }else {
                 /* startActivity(new Intent(mActivity,dashboardActivity.class));*/
                    AppUtils.hideSoftKeyboard(mActivity);
                    if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity))
                    {
                        AppUtils.showRequestDialog(mActivity);
                        hitForgetApi();
                    }
                    else
                    {
                        AppUtils.showErrorMessage(etUrName, getString(R.string.no_internet), this.mActivity);
                    }
                }
                break;
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //on KeyBoard  touch out side event...()
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


    /*******************************ForgetApi**********************************/
    private void hitForgetApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AboutApi", AppUrls.forgetpassword);
        AndroidNetworking.post(AppUrls.forgetpassword)
                .addBodyParameter("user_id", AppSettings.getString(AppSettings.userId))
                .addBodyParameter("code",etOTP.getText().toString().trim())
                .addBodyParameter("password",etForgetConfirmOTP.getText().toString().trim())
                .addBodyParameter("auth_key","camsapitaxi")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseForgetApi(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                        Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        // handle error
                        if (error.getErrorCode() != 0) {
                            //  AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            // AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }


    private void parseForgetApi(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            if(response.get("status").equals("success")){
                Toast.makeText(mActivity, ""+response.get("message"), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this,loginActivity.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
